- Créer une application réservation, sur spring initializr
(dépendances web, jpa ou jdbc, h2)
- Ajouter Swagger et Swagger ui (voir cours)
- Créer une api de réservations / reservations
- get /times : retourne la date du jour en UTC
- get /times?timezone=paris : retourn l'heure de Paris

- post /reservations creation d'une reservation :
 - verifier que les dates de debut et fin > now
 - verifier que date debut > date de fin
 - enregistrer dans h2
- get /reservations retourne toutes les reservations lire dans h2

Reservation
id :long
startDate
endDate
destination : string