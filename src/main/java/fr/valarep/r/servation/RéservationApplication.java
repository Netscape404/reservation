package fr.valarep.r.servation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RéservationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RéservationApplication.class, args);
	}

}
