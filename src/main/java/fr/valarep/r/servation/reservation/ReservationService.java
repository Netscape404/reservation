package fr.valarep.r.servation.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    public List<Reservation> getAllReservation() {

        return reservationRepository.findAll();
    }

    public boolean addReservation(Reservation reservation) {

        List<Reservation> reservations = reservationRepository.findByCode(reservation.getCode());

        if (reservations.size() > 0 && dateValidation(reservation)) {
            return false;
        } else {
            reservationRepository.save(reservation);
            return true;
        }
    }

    public boolean dateValidation(Reservation reservation) {

        LocalDateTime currentDate = LocalDateTime.now();

        if (reservation.startDate.isAfter(currentDate)
                && reservation.endDate.isAfter(currentDate)
                && reservation.startDate.isBefore(reservation.endDate))
            return true;

        return false;
    }
}
