package fr.valarep.r.servation.reservation;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    String code;
    LocalDateTime startDate;
    LocalDateTime endDate;
    String destination;
}
