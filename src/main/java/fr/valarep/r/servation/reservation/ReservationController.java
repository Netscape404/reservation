package fr.valarep.r.servation.reservation;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class ReservationController {

    @Autowired
    ReservationService reservationService;

   @GetMapping("/times")
    @ApiOperation(value = "Return current date")
    public ZonedDateTime getCurrentDate(@RequestParam Optional<String> timezone) {

       if (timezone.equals("paris"))
           return  ZonedDateTime.now(ZoneId.of("Europe/Paris"));

       return ZonedDateTime.now(ZoneOffset.UTC);
    }

    @GetMapping("/reservations")
    @ApiOperation(value = "Get all reservations")
    public List<Reservation> getAllPatients() {

        List<Reservation> reservations = new ArrayList<>();
        reservations.addAll(reservationService.getAllReservation());

        return reservations;
    }

    @PostMapping("/reservations")
    @ApiOperation(value = "Add a reservation")
    @ResponseStatus(HttpStatus.CREATED)
    public void addReservation(@RequestBody Reservation reservation) {

        reservationService.addReservation(reservation);
    }
}
